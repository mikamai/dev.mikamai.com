<!-- ## Pimp my TextMate2 — Ruby edition -->
<!--
tags:
- textmate2
- textmate
- ruby
- keyboard
- development environment
- osx
- editor
- code
 -->

Here at Mikamai It's no secret I'm a happy TextMate user and early TM2 adopter. I'm always there if either an editor war is catching fire or if someone needs help setting up his editor.

Above all I still find that TextMate is the best choice for a Ruby developer, even if SublimeText, emacs and vim seem more fashionable these days. Even if I'm saving the full list of reasons for another post I'll tell just this one: **TextMate relies on Ruby for a big part of its implementation that has always been opensource**\*.

Of course I'm talking about bundles, if you're not convinced [look at the code used to align assignments ](https://github.com/textmate/source.tmbundle/blob/master/Commands/Align%20Assignments.tmCommand#L8-L130) (<kbd>⌥⌘]</kbd>) from the [source bundle](https://github.com/textmate/source.tmbundle) (which is responsible for actions common to any programming language).

Just to be clear I would still use SublimeText if I were to program from Linux or (ugh!) Windows.

That said I want to gather here some of the stuff that makes using TextMate2 for Ruby and Rails development so awesome.

<small>\* Of course I know about [redcar](http://redcareditor.com) (which seems quite dead, but I didn't tried it recently) and other TM <del>clones</del> err enhancements like [Chocolat](https://chocolatapp.com)</small>




> ALERT: shameless self promotion follows

## The Bundles and Settings parade


### 1. Effortless opening of Bundled Gems <kbd>⌥⌘O</kbd>

This I do all the time, opening the source of bundled gems. Please behold and don't be horrified. Especially in Ruby-land the source of gems is the best source of documentation and, as [explained by Glenn Vanderburg](http://www.confreaks.com/videos/282-lsrc2010-real-software-engineering), there's probably a good reason for that. Also the README and specs are included most of the time and reading other's code is a healthy activity.

Needless to say that the best place to read source code is your editor.

<kbd>⌥⌘O</kbd> will present the complete list of gems from your `Gemfile.lock`, start typing the first letters of a gem and use arrow if you don't want to touch the mouse (or trackpad).

![opening gems from the current bundle](https://f.cloud.github.com/assets/1051/190195/03670698-7ed5-11e2-983d-6da8b0d0dd7a.png)

> Source: https://github.com/elia/bundler.tmbundle


### 2. Beautiful Markdown rendering

No README.md reading activity would be on par with a the [GFM](https://help.github.com/articles/github-flavored-markdown) rendered version without code blocks highlighting.

This bundle almost looks like GFM while typing, press <kbd>⌃⌥⌘P</kbd> (the standard TM key equivalent for preview) to get it rendered to the HTML window.

![Redcarpet Markdown Bundle in action](http://cl.ly/image/1Y071W2A2l1w/Screen%20Shot%202014-02-18%20at%2011.02.32%20am.png)

**Bonus** [Install the **Scott Web Theme** from *Preferences → Bundles*](http://cl.ly/image/2v3v1Z0u3F11) for a nice looking preview

> Source: https://github.com/streeter/markdown-redcarpet.tmbundle


### 3. Restart Pow! in a single stroke <kbd>⌃⌥⌘R</kbd>

Restarts the current app detecting a `tmp/` directory in current project or in a parent dir.

![Falls back to /tmp](http://cl.ly/image/2b3K3D3w2C11/Screen%20Shot%202014-02-24%20at%2012.24.32%20pm.png)

> Source: https://github.com/elia/pow-server.tmbundle


### 4. Open the terminal in your current project folder

Works with both Terminal and iTerm, just press <kbd>⌃⌥⌘T</kbd> from a project.

> Source: https://github.com/elia/avian-missing.tmbundle


### 5. Trailing whitespace fix, cross-tab completion and more…

Command | Description
--- | ---
<kbd>⌃⎋</kbd> | Cross tab completion
<kbd>⌃⌥⌘T</kbd> | Open Project directory in Terminal
<kbd>⌃⌥⌘L</kbd> | Keep current file as reference

> Source: https://github.com/elia/avian-missing.tmbundle


## Installing the whole thing

Download the latest version of TextMate2 here: https://api.textmate.org/downloads/release


    mkdir -p ~/Library/Application\ Support/Avian/Bundles
    cd ~/Library/Application\ Support/Avian/Bundles

    git clone https://github.com/elia/avian-missing.tmbundle
    git clone https://github.com/elia/bundler.tmbundle
    git clone https://github.com/streeter/markdown-redcarpet.tmbundle

    # Activate the system ruby (if you're using a Ruby version manager):
    type rvm &> /dev/null && rvm use system # for RVM
    export RBENV_VERSION="system"           # for rbenv

    # Install the required gems
    sudo gem install redcarpet -v 2.3.0
    sudo gem install pygments.rb

    # Trailing whitespace
    defaults write com.macromates.TextMate.preview environmentVariables -array-add \
        '{ "enabled" = YES; "name" = "TM_STRIP_WHITESPACE_ON_SAVE"; "value" = "true"; }' # enable trailing whitespace removal and EOF fix

    echo <<-INI >> ~/.tm_properties
    [ "*.y{,a}ml" ]
    # Disable trailing whitespace fix for YAML
    # files that can be broken by this feat.
    TM_STRIP_WHITESPACE_ON_SAVE = false
    INI

The following will set the tabs/file-browser/html-windows to my current taste, I don't pretend it matches everyone prefs but can still be useful for cherry-picking.


    # File browser fixes and general UI fixes
    defaults write com.macromates.TextMate.preview fileBrowserStyle SourceList       # lighblue file browser background
    defaults write com.macromates.TextMate.preview fileBrowserPlacement left         # keep it on the left
    defaults write com.macromates.TextMate.preview tabsAboveDocument -bool YES       # no tabs above the file browser
    defaults write com.macromates.TextMate.preview allowExpandingLinks -bool YES     # make symlinks expandable
    defaults write com.macromates.TextMate.preview htmlOutputPlacement right         # place the html output to the right
    defaults write com.macromates.TextMate.preview disableTabBarCollapsing -bool YES # keep the tab-bar alway visible
    defaults write com.macromates.TextMate.preview scrollPastEnd -bool YES           # give me some air after the file ends
