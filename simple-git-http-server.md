# Simple Git HTTP server

The other day I had to share a repo to a colleague without him having access to the online repo.

Conscious that git can serve repositories through HTTP I set myself to discover the simplest way to do it without a full blown git hosting app like GitLab or Gitorious.

At that point I took out my google-fu and came up with the following:

```bash
git update-server-info # this will prepare your repo to be served
ruby -run -ehttpd -- . -p 5000
git clone http://localhost:5000/.git repo_name
```

Now by just knowing your IP anyone will be able to clone that repo.

**BONUS**

If you're a PHP nostalgic OSX 10.10 comes with the `php` command which is able to serve a directory and interpret any PHP file in it (like `mod_php` would):

```bash
git update-server-info # this will prepare your repo to be served
php -S 0.0.0.0:5000 -t .
git clone http://localhost:5000/.git repo_name
```


_Stay tuned for more PHP **and** Ruby awesomeness!_
