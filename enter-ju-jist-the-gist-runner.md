# Enter ju-jist, the gist runner

> Neo: Ju jitsu? I'm gonna learn Ju jitsu?

## Forewords

In the [last article][d3-opal] we explored the internals of [D3.js][d3], the data visualization library.
In doing that we ended up with a number of files in [a gist][opal-d3-gist].

This morning I thought it'd be nice to put up the thing into one of those JavaScript *fiddle* sites, I looked up a bunch of them: [CodePen](codepen.io), [JSFiddle](http://jsfiddle.net), [JS Bin](http://jsbin.com) but none of them allowed for arbitrary extensions or loading a code from a Gist[^1].

*I had to build my own.*

### The Plan

1. load and run gists by visiting URLs in this form: `http://ju-jist.herokuapp.com/USER/GIST_ID/FILE_NAME`
2. eventually add a home page that will build the right url from the `GIST_ID` defaulting to `index.html` as file name

## Section 1: Rack Proxy

The first thing I did is to preparing a simple proxy rack application that would extract the *gist id* and
*file name* from the URL:

```ruby
parts = %r{^/(?<user>[^/]+)/?(?<gist_id>\w+)/(?<file>.*)(?:$|\?(?<query>.*$))}.match(env['PATH_INFO'])
gist_id = parts[:gist_id]
file = parts[:file]
user = parts[:user]
```

Note here how handy are actually [named Regexp groups][named-regexp] (introduced in Ruby 1.9).

Then let's be *ol' school* and use [`open-uri`][open-uri] to fetch urls:

```ruby
contents = open("https://gist.githubusercontent.com/#{user}/#{gist_id}/raw/#{file}")
```

Pass it over to Rack:

```ruby
[200, {}, [contents.read]]
```

And wrap everything in a rack app:

```ruby
# config.ru
def call(env)
  parts = %r{^/(?<user>[^/]+)/?(?<gist_id>\w+)/(?<file>.*)(?:$|\?(?<query>.*$))}.match(env['PATH_INFO'])
  gist_id = parts[:gist_id]
  file = parts[:file]
  user = parts[:user]
  contents = open("https://gist.githubusercontent.com/#{user}/#{gist_id}/raw/#{file}")
  [200, {}, [contents.read]]
end

run self
```



## Section 2: The URL builder

Next I prepared a simple form:

```html
<!-- inside index.html -->
<form>
  <input type="text" id="user_and_gist_id">
  <input type="submit" value="open">
</form>
```

And then I used [Opal][opal] and [Native][native-article] with some vanilla DOM to build the URL and
redirect the user.

```ruby
# gist-runner.rb
$doc  = $$[:document]
input = $doc.querySelector('input#user_and_gist_id')
form  = $doc.querySelector('form')

form[:onsubmit] = -> submit {
  Native(submit).preventDefault
  user_and_gist_id = input[:value]
  $$[:location][:href] = "/#{user_and_gist_id}/index.html"
}
```

And let Rack serve static files:

```ruby
use Rack::Static, urls: %w[/gist-runner.rb /favicon.ico], index: 'index.html'
run self
```


## Conclusion

[Ju-Jist is up and running][ju-jist], you can see the code from the [last article gist][d3-opal] live [on ju-jist](http://ju-jist.herokuapp.com/elia/88a4554653df42a6f7c3/index.html).

The code is [available on GitHub][ju-jist-code].


[^1]: Actually JSFiddle has some docs for loading Gists, but I wasn't able to make it work. CodePen and others allow for external resources, but GitHub [blocks the sourcing of raw contents from Gists][github-cors].

[d3-opal]: http://dev.mikamai.com/post/87806500959/learning-d3-js-basics-with-ruby-and-opal
[d3]: http://d3js.org
[opal-d3-gist]: https://gist.github.com/elia/88a4554653df42a6f7c3
[github-cors]: https://github.com/blog/1482-heads-up-nosniff-header-support-coming-to-chrome-and-firefox
[named-regexp]: http://mikepackdev.com/blog_posts/4-tuesday-tricks-named-regex-groups
[open-uri]: http://elia.schito.me/railsapi.com/public/doc/ruby-v2.0/classes/OpenURI.html
[opal]: http://opalrb.org
[native-article]: http://dev.mikamai.com/post/79398725537/using-native-javascript-objects-from-opal
[ju-jist]: http://ju-jist.herokuapp.com
[ju-jist-code]: https://github.com/mikamai/ju-jist
