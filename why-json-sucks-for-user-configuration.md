# Why JSON sucks for user configuration

_In the current *era of JavaScript* [JSON](http://json.org) has become super common. In this article I want to make a point about it not being suitable for user configuration._


## The good

Let's start with _the good parts_. It's not XML. Being born as a data interchange format from the mind of [Douglas Crockford][dc] it shines for its simplicity in both the definition and easiness in parsing. That's pretty much it. As a configuration tool the only good thing I can say is that many people are accustomed to it, and *usually* already know its rules.


## The bad

As popular as it is the JSON format has started to be overused, especially for letting users to configure stuff (that's the point of the article). The first thing to notice here is that JSON is not intended to be used that way, instead we can quote from the official page:

> JSON (JavaScript Object Notation) is a lightweight data-interchange format. It is easy for humans to read and write. It is easy for machines to parse and generate.

*source: http://json.org*

What I'd like to highlight is that JSON is a compromise between write/readability **for both humans and machines**. A data interchange format can easily be almost unreadable to humans, that happens all the time with binary formats **but** a configuration format should instead be biased towards humans.


## The ugly

_But what's wrong with JSON?_

I see three things that make JSON fail as a configuration format:

### Strictness

The format is really demanding. Contrary to the ordinary JavaScript literal object format (which is where it comes from) JSON mandates **double quotes around keys** and **strict absence of commas before closed parentheses**.


### Lack of comments

The absence of comments is perfectly fine in a data interchange formats where the payload will probably travel on wires and needs to be as tiny as possible. In a configuration format conversely it's plain non-sense as they can be used to help the user explaining each configuration option or for the user to explain why he chose a particular option.


### Readability

While JSON is quite readable when properly formatted, this good practice is not enforced by the format itself, so it's up to coders good will to add returns, tabs and spaces in the right places. Lucky enough they usually do.


## Conclusions

_JSON has become very popular thanks to JavaScript to the point that is also a [super][sublime] [common][chef] [format][heroku] for user configuration. It wasn't designed to do so and in fact it does an awful job._

Almost anything is a good alternative, from [INI][ini] to [CSON][cson]. The one I like the most tho is YAML (which in turn has been erroneously used as a data interchange format). Here is the YAML tagline:

> YAML is a human friendly data serialization standard for all programming languages.

*source: http://yaml.org*


[sublime]: http://www.sublimetext.com/docs/3/settings.html
[chef]: http://leopard.in.ua/2013/01/07/chef-solo-getting-started-part-3/
[heroku]: https://www.expeditedssl.com/heroku-button-maker
[dc]: https://en.wikipedia.org/wiki/Douglas_Crockford
[ini]: https://en.wikipedia.org/wiki/INI_file
[cson]: https://github.com/bevry/cson#readme
