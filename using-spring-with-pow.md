# Using spring with pow!

The other day I was a bit sad for I was aware that [pow][] (the web server) wasn't leveraging [spring][] (the Rails preloader) with its fast load times.

## Fixing config.ru

Luckily this deficiency is easily fixed by adding the spring snippet to your `config.ru`:

```ruby
# This file is used by Rack-based servers to start the application.

begin
  load File.expand_path('../bin/spring', __FILE__)
rescue LoadError
end

require ::File.expand_path('../config/environment',  __FILE__)
run Rails.application
```

[pow]: (http://pow.cx)
[spring]: https://github.com/rails/spring
