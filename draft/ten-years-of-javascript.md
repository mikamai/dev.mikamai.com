# Ten (plus) Years of JavaScript

## state of the art, circa 2003

http://yehudakatz.com/2008/08/18/method_missing-in-javascript/



## current status: missing
## why you need `method_missing`

Every JavaScript developer knows this error message: `TypeError: 'undefined' is not a function`






## oops, we did it again

So, if you hoped for a moment I wouldn't have talked about [Opal][opal] you're out of luck.

Actually turns out that Opal has a pretty good implementation of method missing. Let's look at how it works.




### Ruby-land

Contrarialy to what most people thinks when the Ruby parser looks at your code it has a clear understanding of what is a local variable and what is a function. This is commonly believed because to a human eye the [look identical][barewords].

### JS-land

As seen above we're still stuck on method missing stuff,



Every time Opal compiles a piece of Ruby code it will keep a list of all the method calls it finds inside it. The list will then be used to add `method_missing` catchers to `BasicObject` (the root Ruby object).

At this point when a method is called on a JavaScript object it will walk the prototype chain and reach the catcher in the root object. For example:

```ruby
foo = Object.new # foo is a local variable
bar              # bar is a method call on the main object
```

This code has in it two method calls: `new` and `bar`. We can see how the compilers stubs those calls inside `BasicObject`:

```js
$opal.add_stubs(['$new', '$bar']);
foo = $scope.Object.$new();
self.$bar();
```

So, what happens when `self.$bar()` is called?

```
      BasicObject.prototype.$bar  → self.$method_missing('$bar')
                  ↑
        Object.prototype.$bar
                  ↑
        Object.prototype.$bar
                  ↑                             ↓
          (self metaclass)
                  ↑
              self.$bar()             self.$method_missing
```







[opal](http://opalrb.org)
[barewords](http://devblog.avdi.org/2012/10/01/barewords/)
