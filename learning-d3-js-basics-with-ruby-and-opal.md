# Learning D3.js basics with Ruby (and Opal)

*I always wanted to learn D3.js, problem is JavaScript is too awesome and that kept turning me off… till now!*

Let's take a random tutorial that we will attempt to translate into Ruby, for example this: http://bl.ocks.org/mbostock/3883245.

A couple of thing we need to bear in mind are these: D3 is not object-oriented and it's a pretty complex library. We're expecting some problems.

To have something to compare it to we may note, for example, that JQuery is basically a class (`$`) that exposes methods that returns other instances of the same class. That makes things easy enough when we try to use it from an OO language like Ruby.

## Part I — The Setup

Following the tutorial, let's setup the HTML page:

```html
<!DOCTYPE html>
<html>
<meta charset="utf-8">
<style>
/* …uninteresting CSS from the tutorial here… */
</style>
<body>
<script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>

<!-- OUR ADDITIONS HERE -->
<script src="http://cdn.opalrb.org/opal/current/opal.js"></script>
<script src="http://cdn.opalrb.org/opal/current/native.js"></script>
<script src="http://cdn.opalrb.org/opal/current/opal-parser.js"></script>

<script type="text/ruby" src="./app.rb"></script>

</body>
</html>
```

As you probably have noted we added a couple of libraries from the [Opal CDN][cdn].

First we added `opal.js`, that is the runtime and core library that are necessary to run code compiled with Opal.

Then there's `native.js` that we'll use to interact with native objects ([more details in this other post][native-article]).

And last we have `opal-parser.js` and `app.rb` (that is declared as `type="text/ruby"`). The parser will look for all script tags marked as `text/ruby` and will load, parse and run them.

In additon we're also providing a `data.tsv` file as described in the tutorial.


## Part II — Code Ungarbling

**About the process of translating**

The approach I used to translate this code is quite simple, I copy/pasted all the JavaScript from the tutorial into `app.rb` and commented it out. Then I translated one line (or sentence) at a time tackling errors as they came.


### Using Native

The first thing we need to do is to wrap the `d3` global object with `Native`, so that is ready for Ruby consumption.

```ruby
d3 = Native(`window.d3`)
```

Now let's look at the first chunk of code

**original code:**

```js
var parseDate = d3.time.format("%d-%b-%y").parse;

var x = d3.time.scale()
    .range([0, width]);

var y = d3.scale.linear()
    .range([height, 0]);

var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom");

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left");

```

**converted code**

```ruby
d3 = Native(`window.d3`)
time  = d3[:time]
scale = d3[:scale]
svg   = d3[:svg]

date_parser = time.format("%d-%b-%y")

x = time.scale.range([0, width])
y = scale.linear.range([height, 0])

x_axis = svg.axis.scale(x).orient(:bottom)
y_axis = svg.axis.scale(y).orient(:left)
```

By trying to run this code we discover early on that somehow `Native` is failing to deliver calls via method missing.

The reason is that bridged classes are a kind of their own and turns out that D3 tends to return augmented anonymous functions and arrays. Now both `Array` and `Proc` are bridged classes. That means that they are the same as their JS counterparts: `Array` and `Function`.

`Native` will have no effect on bridged classes (no wrapping) and therefore all calls to properties added by D3 will end in calls on `undefined`.

To solve this we'll manually expose the methods on those classes, the final code looks like this:

```ruby
module Native::Exposer
  def expose(*methods)
    methods.each do |name|
      define_method name do |*args,&block|
        args << block if block_given?
        `return #{self}[#{name}].apply(#{self},#{args.to_n})`
      end
    end
  end
end

Proc.extend Native::Exposer
Array.extend Native::Exposer

Proc.expose :range, :axis, :scale, :orient, :line, :x, :y, :parse, :domain
Array.expose :append, :attr, :call, :style, :text, :datum
```

The complete code can be found in [this gist][code].

**UPDATE:** [see the code in action on Ju-Jist][ju-jist], and [read about building ju-jist in this article][ju-jist-article]

### A better solution

Let's hope that in the future the Opal team (me included) will add method missing stubs[^1] to all bridged classes (instead of just adding them `BasicObject`).

[^1]: (which is the underlying mechanism that powers `method_missing` support in Opal)



## Conclusion

We probably didn't learn very much about how to use D3.js, but we discovered a bit of its internals, exposing an interesting style of JavaScript.








[CDN]: http://cdn.opalrb.org/
[code]: https://gist.github.com/elia/88a4554653df42a6f7c3
[native-article]: http://dev.mikamai.com/post/79398725537/using-native-javascript-objects-from-opal
[ju-jist]: http://ju-jist.herokuapp.com/elia/88a4554653df42a6f7c3/index.html
[ju-jist-article]: http://dev.mikamai.com/post/87984833354/enter-ju-jist-the-gist-runner
